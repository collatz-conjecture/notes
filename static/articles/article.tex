\documentclass{article}

\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}

\theoremstyle{definition}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{example}{Example}

\begin{document}

\title{Collatz conjecture notes}

\author{Gilles Rasigade}

\maketitle              % typeset the title of the contribution


% You don't need an abstract or keywords for an article review
%\begin{abstract}
%The abstract should summarize the contents of the paper
%using at least 70 and at most 150 words. It will be set in 9-point
%font size and be inset 1.0 cm from the right and left margins.
%There will be two blank lines before and after the Abstract. \dots
%\keywords{List up to three keywords here, like this:
%computational geometry, graph theory, Hamilton cycles}
%\end{abstract}


% TO MAKE A TITLE PAGE USE THE FOLLOWING COMMAND HERE.
% \newpage




\section{Introduction}

We are gathering in this article a few notes and results related to the Collatz conjecture.

\section{Summary}

The summary section of a critical review should be very short. It is usually between a paragraph and a page, depending on the length of the article. The purpose of the summary is not to provide every single detail of the work, but rather to provide the reader with an overview of the main arguments and structure.

\section{Problem definition}

\begin{definition}[Problem]

Applying a finite number of time the equation \ref{eq:collatz-conjecture}, the final number will land on the cycle $4-2-1$.

\begin{equation} \label{eq:collatz-conjecture}
  f(n) =
  \begin{cases}
      n/2 & \text{if }n \equiv 0 \mod 2 \\
      3n+1 & \text{if }n \equiv 1 \mod 2
  \end{cases}
\end{equation}

\end{definition}

\section{Problem simplification}

\subsection{Even numbers}

Because even numbers in the context of the Collatz conjecture equation are falling down to a lowered value of an odd number, we can remove these numbers from the analysis. In the following of this article, we will integrate these even numbers in the concept of generators which are basically numbers of the form: a odd number multiplied by a power of $2$.

\section{Generators}

\subsection{Definition}

\begin{definition}[Generator]

A $Generator$ is a function of the following form:

\begin{equation} \label{generator-definition}
  \begin{split}
  \mathcal{G}_a :\ & \mathbb{N} \rightarrow \mathbb{N} \\
                  & a \rightarrow a 2^n ,n \in \mathbb{N}, \space \text{a odd}
  \end{split}
\end{equation}

\end{definition}

Applying the Equation \ref{eq:collatz-conjecture} to $G_a$ would lead to $a$:

\begin{equation}
  f\left(G_a\right) = a
\end{equation}

\subsection{Neutral element}



\begin{theorem}

  The shape of the $Generator$ accepts a single neutral element $G_1$.

\end{theorem}

\begin{proof}

\begin{align*}
    f(\mathcal{G}_a) & = \mathcal{G}_a \in \mathbb{N} \\
    3 \times a 2^p + 1 & = a 2^q \implies q > p \\
    a \left( 2^q - 3 \times 2^p \right) & = 1 \\
    a 2^p \left( 2^{q-p} - 3 \right) & = 1 \\
    a & = \frac{1}{2^p \left( 2^{q-p} - 3 \right)} \\
    a, p, n \in \mathbb{N} \implies a & = 1
\end{align*}

So the only neutral element for the Collatz conjecture is $G_1$.

\end{proof}

\subsection{Children of a generator}

What are odd numbers leading to $b$ with applying the equation \ref{eq:collatz-conjecture} ?

\begin{align}
  f\left(a\right) & = b        \\
     \frac{3a + 1}{2^n} & = b  \\
     a & = \frac{ b 2^n - 1}{3} \label{eq:collatz-inverse}
\end{align}

We know that $2^n - 1$ is divisible by $3$ if $n$ is even; and $2^n + 1$ is divisible by $3$ if $n$ is odd.

\begin{theorem}
  $3k$ numbers have no children by the application of the reverse Syracuse equation.
\end{theorem}

\begin{proof}

For values $b$ of the form $3k$, equation \ref{eq:collatz-inverse} is evolving the following way:

\begin{align}
  a & = \frac{ b 2^n - 1}{3} \\
  a & = \frac{ 3k 2^n - 1}{3} \\
  a & = k 2^n - \frac{1}{3}
\end{align}

This last equation does not accept any solution in $\mathbb{N}$ so odd numbers of the form $b = 3k$ where $k \in \mathbb{N^*}$ do not have any children by the application of the reverse Syracuse equation \ref{eq:collatz-inverse}.

\end{proof}

\begin{theorem}
  $3k - 2$ numbers have children if and only if $n$ is even.
\end{theorem}

\begin{proof}

  For values $b$ of the form $3k - 2$, equation \ref{eq:collatz-inverse} is evolving the following way:

\begin{align}
  a & = \frac{ b 2^n - 1}{3} \\
  a & = \frac{ \left( 3k - 2 \right) 2^n - 1}{3} \\
  a & = \frac{ 3k 2^n - 2^{n+1} - 1}{3} \\
  a & = k2^n - \frac{ 2^{n+1} + 1}{3}
\end{align}

The last equation is accepting solution in $\mathbb{N}$ if and only if $n \in 2\mathbb{N}$.

\end{proof}

\begin{theorem}
  $3k + 2$ numbers have children if and only if $n$ is odd.
\end{theorem}

\begin{proof}

  For values $b$ of the form $3k + 2$, equation \ref{eq:collatz-inverse} is evolving the following way:

\begin{align}
  a & = \frac{ b 2^n + 1}{3} \\
  a & = \frac{ \left( 3k + 2 \right) 2^n - 1}{3} \\
  a & = \frac{ 3k 2^n + 2^{n+1} - 1}{3} \\
  a & = k2^n + \frac{ 2^{n+1} - 1}{3}
\end{align}

The last equation is accepting solution in $\mathbb{N}$ if and only if $n \in 2\mathbb{N}+1$.

\end{proof}

\subsection{Mapping of $\mathbb{N}$ space}

If we are displaying on a map where on abscissa are located every odd numbers in order; and in ordinate powers of $2$ of the equation \ref{eq:collatz-inverse}; we can extract some interesting properties related to this new mapping.

\begin{theorem}
  Vertical movements are quantified by $b2^n$
\end{theorem}

\begin{proof}

  Vertical movements are for a single value of $b$, the odd number that will be reached after the application of the Syracuse eqation.

\begin{align}
  \frac{ b 2^p - 1 }{ 3 } - \frac{ b 2^n - 1 }{ 3 }    
                               &= \frac{ b (2^p - 2^n) }{ 3 } & n < p \\
  \delta^n_{b, \downarrow}     & = \frac{ b 2^n (2^{p-n} - 1) }{ 3 } \\
  \delta^{n,v}_{b, \downarrow}    & = \frac{ b 2^n (2^{n + 2 v -n } - 1) }{ 3 } & p = n + 2v \\
  \delta^{n,v}_{b, \downarrow}    & = \frac{ b 2^n (2^{2v} - 1) }{ 3 }
\end{align}

This means that a single vertical movement towards higher power values of $2$ is equal to:

\begin{align}
  \delta^{n,1}_{b, \downarrow}    & = b2^n
\end{align}

\begin{example}
  $\delta^{3,1}_{17, \downarrow}= 136$
\end{example}

\end{proof}

\begin{theorem}
  Horizontal movements are quantified by $h2^{n+1}$
\end{theorem}

\begin{proof}

\begin{align}
  \frac{ c 2^n - 1 }{ 3 } - \frac{ b 2^n - 1 }{ 3 }
                                  & = \frac{ 2^n (c - b) }{ 3 } & b < c \\
    \delta^n_{b, h\rightarrow}    & = \frac{ 2^n (2 \times 3 h + b - b )}{ 3 } & c = b + 2 \times 3 h \\
    \delta^n_{b, h\rightarrow}    & = 2^n (2 \times h) \\
    \delta^n_{h\rightarrow}       & = h 2^{n+1}
\end{align}

\begin{example}
  $\delta^2_{1\rightarrow} = 8 = 41 - 33$ where $c = 31$ and $b = 25$
\end{example}

\end{proof}

% \begin{thebibliography}
%
% \bibitem {clar:eke}
% Clarke, F., Ekeland, I.:
% Nonlinear oscillations and
% boundary-value problems for Hamiltonian systems.
% {\it Arch. Rat. Mech. Anal.} 78, 315--333 (1982)
% \end{thebibliography}

\end{document}