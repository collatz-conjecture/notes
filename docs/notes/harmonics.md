---
sidebar_label: Harmonics
sidebar_position: 3
---

> Do we have a progression, a regularity, q frequency
> analysis available for the Collatz problem?

We have $$2/3$$ of the odd numbers which are producing
children with the inverse function $$f^{-1}$$. The numbers
of the form:

$$

    a = 3 k \pm 2, k \in \mathbb{N^*}

$$

Only the numbers produced with a $$n$$ value equals to $$1$$
are producing numbers greater than their own value:

<iframe width="100%" height="140" src="https://docs.google.com/spreadsheets/u/1/d/e/2PACX-1vRDHlbfXQ7_mVQwbmwv7PvTmVeZuuHFZx4huVnfbE8ZQh8kSjKQMe5D8RoY0Q0HzWa1iRUQrjUEAVSB/pubhtml?gid=710177157&single=true&widget=false&headers=false&range=B13:V23&chrome=false"></iframe>

## 3k - 2 (n = 2q)

The movement of one iteration for the odd numbers of the
form $$3k - 2$$ is:

$$
\begin{aligned}

    (3k - 2) - f^{-1}( 3k - 2 ) &=
        3k - 2 - \left( k 2^n - \frac{  2^{n+1} + 1 }{ 3 } \right) \\

        &= k \left( 3 - 2^n \right) + \frac{  2^{n+1} + 1 }{ 3 } - 2 & \text{, } k \in \mathbb{N^*} \text{, } n \in \mathbb{N}

\end{aligned}
$$

Examples:

- $$k = 1$$, $$n = 2$$, $$b = 1$$, $$m = -1 + 3 - 2 = 0$$ (neutral element)
- $$k = 3$$, $$n = 2$$, $$b = 7$$, $$m = -3 + 3 - 2 = -2$$
- $$k = 5$$, $$n = 2$$, $$b = 13$$, $$m = -k + 1 = -4$$
- $$k = 7$$, $$n = 2$$, $$b = 19$$, $$m = -k + 1 = -6$$

For numbers of the form $$3k - 2$$, the minimal contribution
is of $$-k + 1$$ where $$k \in \mathbb{N^*}$$.


## 3k + 2 (n = 2q + 1)

The movement of one iteration for the odd numbers of the
form $$3k + 2$$ is:

$$
\begin{aligned}

    (3k + 2) - f^{-1}( 3k + 2 ) &=
        3k + 2 - \left( k 2^n + \frac{  2^{n+1} - 1 }{ 3 } \right) \\

        &= k \left( 3 - 2^n \right) - \frac{  2^{n+1} - 1 }{ 3 } + 2 & \text{, } k \in \mathbb{N^*} \text{, } n \in \mathbb{N}

\end{aligned}
$$

Examples:

- $$k = 1$$, $$n = 1$$, $$b = 5$$, $$m = 1 - 1 + 2 = 2$$
- $$k = 3$$, $$n = 1$$, $$b = 11$$, $$m = 3 - 1 + 2 = 4$$
- $$k = 5$$, $$n = 1$$, $$b = 17$$, $$m = k + 1 = 6$$
- $$k = 7$$, $$n = 1$$, $$b = 23$$, $$m = k + 1 = 8$$

## Result

Only odd numbers of the form:

$$
\begin{aligned}

    &= \frac{ (3k + 2) 2 - 1 }{ 3 }
    &= \frac{ 6k + 3 }{ 3 }
    &= 2k + 1 \text{, } k \in \mathbb{N^*}

\end{aligned}
$$

In other words $$3, 7, 11, 15, 19, 23$$ are producing
numbers greaters than themselvers in a propensity
of $$2k + 1$$. All other odd numbers are producing a
movement in direction of lower values.

> Do we have a general behaviour?

