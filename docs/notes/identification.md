---
sidebar_label: Identification
sidebar_position: 3
---

> For every $$n \in \mathbb{N}$$, every fly to $$1$$ can be 
> identified as a unique sequence of powers of $$2$$.

> The powers of $$2$$ can be mapped in base 2 because
> numbers with powers of 2 odd or even are included in the
> same parent generator function (order relationship to
> be demonstrated obviously).

|   a   |  power  |  fly  |         sequence         |
| :---: | :-----: | :---: | :----------------------: |
|   1   |    2    |   1   |          1 -> 1          |
|   5   |    4    |   1   |          5 -> 1          |
|   3   |   4-1   |   2   |       3 -> 5 -> 1        |
|  13   |   4-3   |   2   |       13 -> 5 -> 1       |
|  17   |  4-3-2  |   3   |    17 -> 13 -> 5 -> 1    |
|  11   | 4-3-2-1 |   3   | 11 -> 17 -> 13 -> 5 -> 1 |

|   a   | power binary | $$\in G_i$$ |  fly  |         sequence         |
| :---: | :----------: | :---------: | :---: | :----------------------: |
|   1   |     `0`      |   $$G_1$$   |   1   |          1 -> 1          |
|   5   |     `0`      |   $$G_1$$   |   1   |          5 -> 1          |
|   3   |     `01`     |   $$G_5$$   |   2   |       3 -> 5 -> 1        |
|  13   |     `01`     |   $$G_5$$   |   2   |       13 -> 5 -> 1       |
|  17   |    `010`     | $$G_{13}$$  |   3   |    17 -> 13 -> 5 -> 1    |
|  11   |    `0101`    | $$G_{17}$$  |   3   | 11 -> 17 -> 13 -> 5 -> 1 |
