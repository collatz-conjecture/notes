---
title: Index
sidebar_label: Index
sidebar_position: 0
---

## The problem

$$

f(n) =

\begin{cases}

    n/2 & \text{if }n \equiv 0 \mod 2 \\
    3n+1 & \text{if }n \equiv 1 \mod 2

\end{cases}

$$

The conjecture

> This process will eventually reach the number 1, regardless of which positive integer is chosen initially.

