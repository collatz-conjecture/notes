async function main() {
    const a = 1;
    const b = 1000000;

    const results = new Map();
    const ids = new Map();
    const binaryIds = new Map();
    const tic = Date.now();

    for (let i = a; i < b; i += 2) {
        const powers = [];
        const numbers = [i];

        let current = i;
        let next = i;
        while (next !== 1) {
            if (next % 2 === 0) {
                next = next / 2;
            } else {
                if (false && results.has(next)) {
                    const fragment = results.get(next);

                    // console.log('before:', next, numbers, powers);

                    numbers.push(...fragment.numbers.reverse());
                    powers.push(...fragment.powers.reverse());

                    next = numbers.pop();
                    current = numbers.pop();
                    numbers.push(current);

                    // console.log('after:', current, next, numbers, powers);

                    // return;
                    break;
                }

                if (current !== next) {
                    const power = Math.log2((3 * current + 1) / next);

                    numbers.push(next);
                    powers.push(power);
                }

                current = next;

                next = 3 * next + 1;
            }
        }

        const power = Math.log2((3 * current + 1) / next);
        // console.log(52, { i, current, next, power });

        numbers.push(next);
        powers.push(power);

        const id = powers.reverse().map((p) => `${p}`.padStart(2, '0')).join('-');
        const binaryId = powers.map((p) => `${p % 2}`).join('');
        // ids.set(id, (ids.get(id) || 0) + 1);
        ids.set(id, i);
        // binaryIds.set(binaryId, [...(binaryIds.get(binaryId) || []), i]);
        binaryIds.set(binaryId, binaryIds.get(binaryId) || i);

        results.set(i, {
            fly: powers.length,
            id,
            numbers: numbers.reverse(),
            powers: powers.reverse(),
        });
    }

    // require('fs').writeFileSync('/tmp/ids.json',
    //     JSON.stringify(Object.fromEntries(ids.entries()), null, 2));

    // const sortedIds = Array.from(ids.keys()).sort((a, b) => a.length - b.length || a.localeCompare(b));
    // const sortedBinaryIds = Array.from(binaryIds.keys()).sort((a, b) => a.length - b.length || a.localeCompare(b));
    // const sortedIds = Array.from(ids.keys()).sort((a, b) => a.length - b.length);
    // const sortedBinaryIds = Array.from(binaryIds.keys()).sort((a, b) => a.length - b.length);
    // const sortedIds = Array.from(ids.keys()).sort((a, b) => a.localeCompare(b));
    // const sortedBinaryIds = Array.from(binaryIds.keys()).sort((a, b) => a.localeCompare(b));
    const sortedIds = Array.from(ids.keys());
    const sortedBinaryIds = Array.from(binaryIds.keys());

    require('fs').writeFileSync('/tmp/ids.json',
        JSON.stringify(sortedIds, null, 2));

    // require('fs').writeFileSync('/tmp/binary_ids.json',
    //     JSON.stringify(sortedBinaryIds, null, 2));
    require('fs').writeFileSync('/tmp/binary_ids.csv',
        sortedBinaryIds.map((id) => [id, binaryIds.get(id)].join(',')).join('\n')
    );

    require('fs').writeFileSync('/tmp/numbers.json',
        JSON.stringify(sortedIds.map((id) => ids.get(id)), null, 2));

    require('fs').writeFileSync('/tmp/binary_numbers.json',
        JSON.stringify(sortedBinaryIds.map((id) => binaryIds.get(id)), null, 2));


    console.log('Elapsed:', Date.now() - tic);

    // return Object.fromEntries(ids.entries());
    // return Array.from(ids.keys()).sort();
}

main()
    .then((result) => {
        console.log(result);
    }).catch((err) => {
        console.error(err);

        process.exit(1);
    })