---
title: Generators
sidebar_position: 1
---

The even numbers in the Collatz conjecture does not have
any impact because they are naturally falling down to the
first odd number after $$n$$ divisions by $$2$$.

So, we can work on this problem with only odd numbers thanks
to the definition of the generators.

## Definition

A generator is a function of the form:

$$
\begin{aligned}

    \mathcal{G} :\ & \mathbb{N} \rightarrow \mathbb{N} \\
                   & a \rightarrow a \times 2^n ,n \in \mathbb{N}, \space \text{a odd}

\end{aligned}
$$

It is defined as a generator for the Syracuse problem because any number leading to a function of this form will fall down to $$a$$ after $$n$$ iterations.

## Notation

The notation is then

$$
    G_a = a \times 2^n, n \in \mathbb{N}
$$

## Examples

| $$G_a$$ |                 | $$n \in G_a$$        |
| ------: | :-------------- | :------------------- |
|       1 | $$2^n$$         | 2, 4, 8, 16, 32, ... |
|       3 | $$3 \times2^n$$ | 6, 12, 24, 48, ...   |
|       5 | $$5 \times2^n$$ | 10, 20, 40, 80, ...  |
|       7 | $$7 \times2^n$$ | 14, 28, 56, 112, ... |

## Properties

### Neutral element

The _generator_ $$\mathcal{G}_1$$ is a neutral element for the function $$f$$:

$$
f(\mathcal{G}_1) = \mathcal{G}_1
$$

with

$$
\mathcal{G}_1 = 2^p, p \in \mathbb{N}
$$

#### Demonstration

$$
\begin{aligned}
f(\mathcal{G}_a) & = & \mathcal{G}_a & \in \mathbb{N}\\
3 \times a 2^p + 1 & = & a 2^q & => q > p\\
a \left( 2^q - 3 \times 2^p \right) & = & 1\\
a 2^p \left( 2^{q-p} - 3 \right) & = & 1\\
a & = & \frac{1}{2^p \left( 2^{q-p} - 3 \right)}\\
a, p, n \in \mathbb{N} & => & a = 1 &
\end{aligned}
$$

:::tip Questions

- Do we have other neutral elements for the function $$f$$?
- Is there a relation between neutral elements and cycles for the function $$f$$?

:::

### Children of an odd number

Let's have a number $$b \in \mathbb{N}$$ and **odd** number. What are the other odd numbers for which the application of $$f$$ leads to $$b$$?

$$
\begin{aligned}

    b 2^n = f(c_b) & = & 3 c_b + 1 \\
            c_b & = & \frac{ b 2^n - 1 }{ 3 }

\end{aligned}
$$

### Inverse function

We can then define the inverse function of the Collatz
conjecture on odd numbers by:

$$
\begin{aligned}

    \mathcal{f^{-1}} :\ & \mathbb{N} \rightarrow \mathbb{N} \\
                   & b \rightarrow \frac{ b 2^n - 1 }{ 3 } ,n \in \mathbb{N}, \space \text{b odd}

\end{aligned}
$$

Examples:

|   b | $$c_b$$            |
| --: | :----------------- |
|   1 | 1, 5, 21, 85, 3413 |
|   3 | ?                  |
|   5 | 3, 13, 53, 213     |
|   7 | 9, 37, 149, 597    |
|   9 | ?                  |
|  11 | 7, 29, 117         |
